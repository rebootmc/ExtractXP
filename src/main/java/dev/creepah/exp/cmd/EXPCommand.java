package dev.creepah.exp.cmd;

import dev.creepah.exp.ExtractXP;
import me.twister915.corelite.command.*;
import me.twister915.corelite.util.ItemShorthand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@CommandPermission("exp.use")
@CommandMeta(description = "Base command for ExtractXP")
public class EXPCommand extends ECommand {

    public EXPCommand() {
        super("exp");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) {
            player.sendMessage(ExtractXP.getInstance().formatAt("exp-levels").with("levels", player.getLevel()).get());
            return;
        }

        if (args[0].equalsIgnoreCase("pay")) {
            if (args.length > 2) {
                Player target = Bukkit.getPlayer(args[1]);
                int levels = parse(args[2]);

                if (target == null) throw new CommandException("The player " + args[1] + " could not be found!");
                if (levels < 1) throw new CommandException("Invalid level specified!");

                if (levels > player.getLevel()) throw new CommandException("You don't have enough levels to do this!");

                player.setLevel(player.getLevel() - levels);
                target.setLevel(target.getLevel() + levels);

                player.sendMessage(ExtractXP.getInstance().formatAt("exp-paid")
                        .withModifier("target", target.getName())
                        .withModifier("amount", levels).get());

                target.sendMessage(ExtractXP.getInstance().formatAt("exp-received")
                        .withModifier("player", player.getName())
                        .withModifier("amount", levels).get());

            } else throw new ArgumentRequirementException("Invalid arguments! Usage: /exp pay <player> <amount>");
        }

        if (args[0].equalsIgnoreCase("withdraw") && args.length == 1)
            throw new CommandException("Invalid arguments! Usage: /exp withdraw <amount>");

        if (args[0].equalsIgnoreCase("withdraw") && args.length == 2) {
            if (!player.hasPermission("exp.withdraw"))
                throw new PermissionException("You don't have permission to withdraw XP!");

            int levels = parse(args[1]);
            if (levels == -1) throw new CommandException("Please enter a number to specify level count!");
            if (levels == 0) throw new CommandException("You must withdraw at least 1 level!");
            if (levels > player.getLevel()) throw new CommandException("You don't have that many XP levels to withdraw!");

            player.setLevel(player.getLevel() - levels);
            player.getInventory().addItem(ItemShorthand.setMaterial(Material.EXP_BOTTLE)
                    .setName("&a" + levels + " XP Levels")
                    .setLore("&7Break this bottle or")
                    .setLore("&7use /exp deposit to get XP")
                    .get());

            player.sendMessage(ExtractXP.getInstance().formatAt("exp-withdrawn").with("levels", levels).get());
        }

        if (args[0].equalsIgnoreCase("deposit")) {
            if (!player.hasPermission("exp.deposit"))
                throw new PermissionException("You don't have permission to deposit XP!");

            if (player.getItemInHand() == null || player.getItemInHand().getType() != Material.EXP_BOTTLE)
                throw new CommandException("You can't deposit that item!");

            ItemStack hand = player.getItemInHand();
            if (!hand.hasItemMeta() || !hand.getItemMeta().hasLore())
                throw new CommandException("The item in your hand is not a special XP bottle!");

            int levels = parse(ChatColor.stripColor(hand.getItemMeta().getDisplayName().replaceAll(" XP Levels", "")));
            if (levels != -1) {
                player.setLevel(player.getLevel() + levels * hand.getAmount());
                player.getInventory().remove(hand);
                player.sendMessage(ExtractXP.getInstance().formatAt("exp-deposited").with("levels", levels * hand.getAmount()).get());
            } else throw new CommandException("Failed to parse that XP bottle. Has the name/lore been modified?");
        }
    }

    public int parse(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ignored) {}
        return -1;
    }
}
