package dev.creepah.exp.cmd;

import dev.creepah.exp.ExtractXP;
import me.twister915.corelite.CLPlayerKnife;
import me.twister915.corelite.command.CLCommand;
import me.twister915.corelite.command.CommandException;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class ECommand extends CLCommand {

    protected ECommand(String name) {
        super(name);
    }

    @Override
    protected void handleCommandException(CommandException ex, String[] args, CommandSender sender) {
        if (sender instanceof Player) CLPlayerKnife.$((Player) sender).playSoundForPlayer(Sound.NOTE_BASS);
        sender.sendMessage(ExtractXP.getInstance().formatAt("error-message").with("message", ex.getMessage()).get());
    }
}
