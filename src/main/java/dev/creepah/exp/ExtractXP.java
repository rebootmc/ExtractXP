package dev.creepah.exp;

import dev.creepah.exp.cmd.EXPCommand;
import lombok.Getter;
import me.twister915.corelite.CLPlayerKnife;
import me.twister915.corelite.plugin.CLPlugin;
import me.twister915.corelite.plugin.UsesFormats;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

@UsesFormats
public final class ExtractXP extends CLPlugin implements Listener {

    @Getter
    private static ExtractXP instance;

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;

        registerCommand(new EXPCommand());
        registerListener(this);
    }

    @EventHandler
    public void onThrow(ProjectileLaunchEvent event) {
        if (event.getEntity() instanceof ThrownExpBottle) {
            ThrownExpBottle expBottle = (ThrownExpBottle) event.getEntity();
            if (expBottle.getShooter() instanceof Player) {
                Player player = (Player) expBottle.getShooter();
                if (player.getItemInHand() != null && player.getItemInHand().getType() == Material.EXP_BOTTLE) {
                    ItemStack hand = player.getItemInHand();
                    if (hand.hasItemMeta() && hand.getItemMeta().hasLore()) {
                        int levels = parse(ChatColor.stripColor(hand.getItemMeta().getDisplayName().replaceAll(" XP Levels", "")));
                        if (levels != -1) {
                            player.setLevel(player.getLevel() + levels);
                            player.getInventory().remove(hand);
                            CLPlayerKnife.$(player).playSoundForPlayer(Sound.ORB_PICKUP);
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getView().getType() == InventoryType.ANVIL) {
            ItemStack item = event.getView().getItem(0);
            if (item != null && item.getType() == Material.EXP_BOTTLE) {
                if (item.hasItemMeta()) {
                    event.setCancelled(true);
                    CLPlayerKnife.$((Player) event.getWhoClicked()).playSoundForPlayer(Sound.NOTE_BASS);
                }
            }
        }
    }

    public int parse(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ignored) {}
        return -1;
    }
}
